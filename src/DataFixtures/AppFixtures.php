<?php

namespace App\DataFixtures;

use App\Entity\Address;
use App\Entity\Document;
use App\Entity\Profession;
use App\Entity\Tva;
use App\Entity\User;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }
    public function load(ObjectManager $manager)

    {
        $testCongif = [

            ['value' => ('Dressage'),       'viewValue' => 'Dressage'],
            ['value' => ("Saut d'obstacle"), 'viewValue' => "Saut d'obstacle"],
            ['value' => ('Complet'),        'viewValue' => 'Complet'],
            ['value' => ('Attelage'),       'viewValue' => 'Attelage'],
            ['value' => ('Volige'),        'viewValue' => 'Voltige'],
            ['value' => ('Trec'),        'viewValue' => 'Trec'],
            ['value' => ('Western'),        'viewValue' => 'Western'],
            ['value' => ("Tir à l'arc"),        'viewValue' => "Tir à l'arc"],
            ['value' => ("Hunter"),        'viewValue' => "Hunter"],
            ['value' => ("Equifun"),        'viewValue' => "Equifun"],
            ['value' => ("Amazone"),        'viewValue' => "Amazone"],
            ['value' => ("Spectacle"),        'viewValue' => "Spectacle"],
            ['value' => ("Pony games"),        'viewValue' => "Pony games"],
            ['value' => ("Horse ball"),        'viewValue' => "Horse ball"],
            ['value' => ("Endurance"),        'viewValue' => "Endurance"],
            ['value' => ("Para-dressage"),        'viewValue' => "Para-dressage"],
            ['value' => ("Ethologie"),        'viewValue' => "Ethologie"]
        ];

        $testHours = [
            ['value' => ('00:30:00'), 'viewValue' => '30 minutes'],
            ['value' => ('01:00:00'), 'viewValue' => '1 heure'],
            ['value' => ('01:30:00'), 'viewValue' => '1 heure 30'],
            ['value' => ('02:00:00'), 'viewValue' => '2 heures'],
            ['value' => ('02:30:00'), 'viewValue' => '2 heures 30']
        ];

        $faker = Faker\Factory::create('fr_FR');

        $profession = new Profession();
        $profession1 = new Profession();
        $profession2 = new Profession();
        $profession3 = new Profession();
        $profession4 = new Profession();
        $profession5 = new Profession();
        $profession6 = new Profession();


        $profession->setName("Moniteur/Coach");
        $profession->setPicture("./assets/img/icones-professions/moniteur.png");
        $profession->setOptionProfession($testCongif);
        $profession->setOptionHour($testHours);
        $manager->persist($profession);


        $profession1->setName("Ostéopathe");
        $profession1->setPicture("./assets/img/icones-professions/osteopathe.png");
        $profession1->setOptionHour($testHours);
        $manager->persist($profession1);

        $profession2->setName("Dentiste");
        $profession2->setPicture("./assets/img/icones-professions/marechal.png");
        $profession2->setOptionHour($testHours);
        $manager->persist($profession2);

        $profession3->setName("Maréchal ferrant");
        $profession3->setPicture("./assets/img/icones-professions/marechal.png");
        $profession3->setOptionHour($testCongif);
        $profession3->setOptionHour($testHours);
        $manager->persist($profession3);

        $profession4->setName("Pareur/Podologue");
        $profession4->setPicture("./assets/img/icones-professions/cavalier.png");
        $profession4->setOptionHour($testHours);
        $manager->persist($profession4);

        $profession5->setName("Ethologue");
        $profession5->setPicture("./assets/img/icones-professions/ethologue.png");
        $profession5->setOptionHour($testHours);
        $manager->persist($profession5);

        $profession6->setName("Shiatsu");
        $profession6->setPicture("./assets/img/icones-professions/cavalier.png");
        $profession6->setOptionHour($testHours);
        $manager->persist($profession6);






        $tva = new Tva();
        $tva->setNameTax("Taux principal");
        $tva->setPercentage(20);
        $manager->persist($tva);

        $tva1 = new Tva();
        $tva1->setNameTax("Taux intermédiaire");
        $tva1->setPercentage(10);
        $manager->persist($tva1);

        $tva2 = new Tva();
        $tva2->setNameTax("Taux reduit");
        $tva2->setPercentage(5.5);
        $manager->persist($tva2);

        // $placeLesson = new PlaceLesson();
        // $placeLesson->setLessonType("A domicile");
        // $manager->persist($placeLesson);

        // $placeLesson1 = new PlaceLesson();
        // $placeLesson1->setLessonType("A l'exterieur");
        // $manager->persist($placeLesson1);

        // $placeLesson2 = new PlaceLesson();
        // $placeLesson2->setLessonType("Webcam");
        // $manager->persist($placeLesson2);

        $user = new User();


        $hashedPassword = $this->encoder->encodePassword($user, '1234');

        $user->setFirstName('florian')
            ->setLastName('maurel')
            ->setPassword($hashedPassword)
            ->setEmail('flo@mail.com')
            ->setDescription($faker->text(200))
            ->setPhone($faker->e164PhoneNumber);



        $manager->persist($user);

        $manager->flush();

        for ($i = 0; $i < 10; $i++) {

            $user = new User();
            $document = new Document();
            $hashedPassword = $this->encoder->encodePassword($user, '1234');

            $user->setFirstName($faker->firstName)
                ->setLastName($faker->lastName)
                ->setPassword($hashedPassword)
                ->setEmail($faker->email)
                ->setDescription($faker->text(200))
                ->setLessonPrice(rand(10, 100))
                ->setPhone($faker->e164PhoneNumber)
                ->setRoles(['ROLE_PARTICULIER'])
                ->addDocument($document);

            $manager->persist($user);

            $document->setUrl($faker->imageUrl(400, 240))
                ->setName("photo")
                ->setExtention("profil");

            $manager->persist($document);
        }
        $manager->flush();

        for ($i = 0; $i < 10; $i++) {

            $user = new User();
            $address = new Address();
            $document = new Document();

            $hashedPassword = $this->encoder->encodePassword($user, '1234');

            $user->setFirstName($faker->firstName)
                ->setLastName($faker->lastName)
                ->setPassword($hashedPassword)
                ->setEmail($faker->email)
                ->setDescription($faker->text(200))
                ->setLessonPrice(rand(10, 100))
                ->setPhone($faker->e164PhoneNumber)
                ->setRoles(['ROLE_PRO'])
                ->addDocument($document);

            $manager->persist($user);



            $address->setZipCode(69100)
                ->setCity($faker->city)
                ->setCountry($faker->country)
                ->setDepartment("Rhone")
                ->setRegion("Rhone alpes")
                ->setStreet($faker->streetAddress)
                ->setUser($user);

            $manager->persist($address);


            $document->setUrl($faker->imageUrl(400, 240))
                ->setName("photo")
                ->setExtention("profil");

            $manager->persist($document);
        }
        $manager->flush();
    }
}
