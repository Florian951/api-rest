<?php

namespace App\Repository;

use App\Entity\Equide;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Equide|null find($id, $lockMode = null, $lockVersion = null)
 * @method Equide|null findOneBy(array $criteria, array $orderBy = null)
 * @method Equide[]    findAll()
 * @method Equide[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EquideRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Equide::class);
    }

    // /**
    //  * @return Equide[] Returns an array of Equide objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Equide
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
