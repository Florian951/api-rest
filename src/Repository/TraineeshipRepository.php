<?php

namespace App\Repository;

use App\Entity\Traineeship;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Traineeship|null find($id, $lockMode = null, $lockVersion = null)
 * @method Traineeship|null findOneBy(array $criteria, array $orderBy = null)
 * @method Traineeship[]    findAll()
 * @method Traineeship[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TraineeshipRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Traineeship::class);
    }

     /**
     * @return Traineeship[]
     */
    public function findTraineeship(string $keyword)
    {

    // on met les "%" avant et après le mot clé pour qu'il cherche ce qui contient mm si il y a des choses avant et apres (doit etre fait à l'exterieur de la requette SQL)
        $key = '%'.$keyword.'%';
        
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            'SELECT traineeship
            FROM App\Entity\Traineeship traineeship
            WHERE traineeship.organizingName LIKE :key 
            OR traineeship.program LIKE :key
            OR traineeship.level LIKE :key'
        )->setParameter('key', $key);

        // returns an array of traineeship objects
        return $query->getResult();
    }

    // /**
    //  * @return Traineeship[] Returns an array of Traineeship objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Traineeship
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
