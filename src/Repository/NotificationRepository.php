<?php

namespace App\Repository;

use App\Entity\Notification;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use phpDocumentor\Reflection\Types\Integer;

/**
 * @method Notification|null find($id, $lockMode = null, $lockVersion = null)
 * @method Notification|null findOneBy(array $criteria, array $orderBy = null)
 * @method Notification[]    findAll()
 * @method Notification[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NotificationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Notification::class);
    }


    // public function findByIdAndRead(integer $id,string $read)
    // {

    // // on met les "%" avant et après le mot clé pour qu'il cherche ce qui contient mm si il y a des choses avant et apres (doit etre fait à l'exterieur de la requette SQL)
    //     $key = '%'.$read.'%';
    //     $id = '%'.$id.'%';
        
    //     $entityManager = $this->getEntityManager();

    //     // on fait une requette "FROM App\Entity\..." et pas directement sur une Table SQL
    //     // il faut donc mettre un alias (ici "product"), pour qu'il la prenne en compte 
    //     // il va la considérer comme une "instance de la table" et chercher dedans.

    //     // là je le fait chercher dans le nom, la categorie et la description, parce que à priori c est les endroits ou un mot clé peut sortir
    //     $query = $entityManager->createQuery(
    //         'SELECT notification
    //         FROM App\Entity\Notification notification
    //         WHERE notification.id LIKE :id
    //         OR notification.notificationRead LIKE :key'
       
    //     )->setParameters(array('key'=> $key, 'id' => $id));

    //     // returns an array of Product objects
    //     return $query->getResult();
    // }

    // /**
    //  * @return Notification[] Returns an array of Notification objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Notification
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
