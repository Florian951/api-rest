<?php

namespace App\Repository;

use App\Entity\Lesson;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Lesson|null find($id, $lockMode = null, $lockVersion = null)
 * @method Lesson|null findOneBy(array $criteria, array $orderBy = null)
 * @method Lesson[]    findAll()
 * @method Lesson[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LessonRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Lesson::class);
    }

   /**
    * @return Lesson[] Returns an array of Lesson objects
    */
    
    public function findEventById( $keyword)
    {

    // on met les "%" avant et après le mot clé pour qu'il cherche ce qui contient mm si il y a des choses avant et apres (doit etre fait à l'exterieur de la requette SQL)
        $key = 1;
        
        $entityManager = $this->getEntityManager();

        // on fait une requette "FROM App\Entity\..." et pas directement sur une Table SQL
        // il faut donc mettre un alias (ici "product"), pour qu'il la prenne en compte 
        // il va la considérer comme une "instance de la table" et chercher dedans.

        // là je le fait chercher dans le nom, la categorie et la description, parce que à priori c est les endroits ou un mot clé peut sortir
        $query = $entityManager->createQuery(
            'SELECT id
            FROM App\Entity\Traineeship traineeship
            WHERE traineeship.id LIKE :key 
            -- ->JOIN App\Entity\Lesson lesson
            -- WHERE lesson.id LIKE :key '
        )->setParameter('key', $key);

        // returns an array of Product objects
        return $query->getResult();
    }
    


    /*
    public function findOneBySomeField($value): ?Lesson
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
