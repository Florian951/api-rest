<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200511084905 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', password VARCHAR(255) NOT NULL, first_name VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, phone INT DEFAULT NULL, lesson_price DOUBLE PRECISION DEFAULT NULL, path DOUBLE PRECISION DEFAULT NULL, level VARCHAR(255) DEFAULT NULL, description VARCHAR(1000) DEFAULT NULL, social_network VARCHAR(350) DEFAULT NULL, facebook VARCHAR(255) DEFAULT NULL, twitter VARCHAR(255) DEFAULT NULL, instagram VARCHAR(255) DEFAULT NULL, web_site VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_comment (user_id INT NOT NULL, comment_id INT NOT NULL, INDEX IDX_CC794C66A76ED395 (user_id), INDEX IDX_CC794C66F8697D13 (comment_id), PRIMARY KEY(user_id, comment_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_user (user_source INT NOT NULL, user_target INT NOT NULL, INDEX IDX_F7129A803AD8644E (user_source), INDEX IDX_F7129A80233D34C1 (user_target), PRIMARY KEY(user_source, user_target)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_traineeship (user_id INT NOT NULL, traineeship_id INT NOT NULL, INDEX IDX_F371478EA76ED395 (user_id), INDEX IDX_F371478E7E55B478 (traineeship_id), PRIMARY KEY(user_id, traineeship_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE traineeship (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, program VARCHAR(400) NOT NULL, level VARCHAR(255) NOT NULL, price DOUBLE PRECISION NOT NULL, end DATETIME NOT NULL, start DATETIME NOT NULL, number_participant INT NOT NULL, title VARCHAR(255) NOT NULL, INDEX IDX_5A6A8E1BA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE equide (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, race VARCHAR(255) DEFAULT NULL, age INT DEFAULT NULL, sexe VARCHAR(255) DEFAULT NULL, date_obtention DATETIME DEFAULT NULL, condition_vie VARCHAR(255) DEFAULT NULL, discipline_pratique VARCHAR(255) DEFAULT NULL, competition TINYINT(1) DEFAULT NULL, competiton_discipline VARCHAR(255) DEFAULT NULL, niveau VARCHAR(255) DEFAULT NULL, cavalier_actuel VARCHAR(255) DEFAULT NULL, objectif VARCHAR(255) DEFAULT NULL, cheval_soin_effectuer VARCHAR(255) DEFAULT NULL, INDEX IDX_AE76D40A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE lesson (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, start DATETIME NOT NULL, end DATETIME NOT NULL, class_name VARCHAR(255) DEFAULT NULL, url VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE lesson_user (lesson_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_B4E2102DCDF80196 (lesson_id), INDEX IDX_B4E2102DA76ED395 (user_id), PRIMARY KEY(lesson_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE document (id INT AUTO_INCREMENT NOT NULL, traineeship_id INT DEFAULT NULL, user_id INT DEFAULT NULL, extention VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, url VARCHAR(255) DEFAULT NULL, type VARCHAR(255) DEFAULT NULL, INDEX IDX_D8698A767E55B478 (traineeship_id), INDEX IDX_D8698A76A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tva (id INT AUTO_INCREMENT NOT NULL, name_tax VARCHAR(255) NOT NULL, percentage DOUBLE PRECISION NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE profession_user (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, price DOUBLE PRECISION NOT NULL, description VARCHAR(255) DEFAULT NULL, hour VARCHAR(32) NOT NULL, INDEX IDX_57812B28A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE profession_user_profession (profession_user_id INT NOT NULL, profession_id INT NOT NULL, INDEX IDX_7CF78577BCD09CE0 (profession_user_id), INDEX IDX_7CF78577FDEF8996 (profession_id), PRIMARY KEY(profession_user_id, profession_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE notification (id INT AUTO_INCREMENT NOT NULL, traineeship_id INT DEFAULT NULL, professional_id INT DEFAULT NULL, creation_date DATETIME NOT NULL, notification_read TINYINT(1) NOT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_BF5476CA7E55B478 (traineeship_id), INDEX IDX_BF5476CADB77003 (professional_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE profession (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, picture VARCHAR(255) NOT NULL, option_profession LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', option_hour LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE address (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, traineeship_id INT DEFAULT NULL, street VARCHAR(255) NOT NULL, city VARCHAR(255) NOT NULL, zip_code INT NOT NULL, country VARCHAR(255) NOT NULL, adress_horse TINYINT(1) DEFAULT NULL, region VARCHAR(255) NOT NULL, department VARCHAR(255) NOT NULL, INDEX IDX_D4E6F81A76ED395 (user_id), INDEX IDX_D4E6F817E55B478 (traineeship_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE comment (id INT AUTO_INCREMENT NOT NULL, note DOUBLE PRECISION DEFAULT NULL, text VARCHAR(350) NOT NULL, date DATETIME NOT NULL, author VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user_comment ADD CONSTRAINT FK_CC794C66A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_comment ADD CONSTRAINT FK_CC794C66F8697D13 FOREIGN KEY (comment_id) REFERENCES comment (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_user ADD CONSTRAINT FK_F7129A803AD8644E FOREIGN KEY (user_source) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_user ADD CONSTRAINT FK_F7129A80233D34C1 FOREIGN KEY (user_target) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_traineeship ADD CONSTRAINT FK_F371478EA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_traineeship ADD CONSTRAINT FK_F371478E7E55B478 FOREIGN KEY (traineeship_id) REFERENCES traineeship (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE traineeship ADD CONSTRAINT FK_5A6A8E1BA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE equide ADD CONSTRAINT FK_AE76D40A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE lesson_user ADD CONSTRAINT FK_B4E2102DCDF80196 FOREIGN KEY (lesson_id) REFERENCES lesson (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE lesson_user ADD CONSTRAINT FK_B4E2102DA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE document ADD CONSTRAINT FK_D8698A767E55B478 FOREIGN KEY (traineeship_id) REFERENCES traineeship (id)');
        $this->addSql('ALTER TABLE document ADD CONSTRAINT FK_D8698A76A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE profession_user ADD CONSTRAINT FK_57812B28A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE profession_user_profession ADD CONSTRAINT FK_7CF78577BCD09CE0 FOREIGN KEY (profession_user_id) REFERENCES profession_user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE profession_user_profession ADD CONSTRAINT FK_7CF78577FDEF8996 FOREIGN KEY (profession_id) REFERENCES profession (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE notification ADD CONSTRAINT FK_BF5476CA7E55B478 FOREIGN KEY (traineeship_id) REFERENCES traineeship (id)');
        $this->addSql('ALTER TABLE notification ADD CONSTRAINT FK_BF5476CADB77003 FOREIGN KEY (professional_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE address ADD CONSTRAINT FK_D4E6F81A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE address ADD CONSTRAINT FK_D4E6F817E55B478 FOREIGN KEY (traineeship_id) REFERENCES traineeship (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_comment DROP FOREIGN KEY FK_CC794C66A76ED395');
        $this->addSql('ALTER TABLE user_user DROP FOREIGN KEY FK_F7129A803AD8644E');
        $this->addSql('ALTER TABLE user_user DROP FOREIGN KEY FK_F7129A80233D34C1');
        $this->addSql('ALTER TABLE user_traineeship DROP FOREIGN KEY FK_F371478EA76ED395');
        $this->addSql('ALTER TABLE traineeship DROP FOREIGN KEY FK_5A6A8E1BA76ED395');
        $this->addSql('ALTER TABLE equide DROP FOREIGN KEY FK_AE76D40A76ED395');
        $this->addSql('ALTER TABLE lesson_user DROP FOREIGN KEY FK_B4E2102DA76ED395');
        $this->addSql('ALTER TABLE document DROP FOREIGN KEY FK_D8698A76A76ED395');
        $this->addSql('ALTER TABLE profession_user DROP FOREIGN KEY FK_57812B28A76ED395');
        $this->addSql('ALTER TABLE notification DROP FOREIGN KEY FK_BF5476CADB77003');
        $this->addSql('ALTER TABLE address DROP FOREIGN KEY FK_D4E6F81A76ED395');
        $this->addSql('ALTER TABLE user_traineeship DROP FOREIGN KEY FK_F371478E7E55B478');
        $this->addSql('ALTER TABLE document DROP FOREIGN KEY FK_D8698A767E55B478');
        $this->addSql('ALTER TABLE notification DROP FOREIGN KEY FK_BF5476CA7E55B478');
        $this->addSql('ALTER TABLE address DROP FOREIGN KEY FK_D4E6F817E55B478');
        $this->addSql('ALTER TABLE lesson_user DROP FOREIGN KEY FK_B4E2102DCDF80196');
        $this->addSql('ALTER TABLE profession_user_profession DROP FOREIGN KEY FK_7CF78577BCD09CE0');
        $this->addSql('ALTER TABLE profession_user_profession DROP FOREIGN KEY FK_7CF78577FDEF8996');
        $this->addSql('ALTER TABLE user_comment DROP FOREIGN KEY FK_CC794C66F8697D13');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE user_comment');
        $this->addSql('DROP TABLE user_user');
        $this->addSql('DROP TABLE user_traineeship');
        $this->addSql('DROP TABLE traineeship');
        $this->addSql('DROP TABLE equide');
        $this->addSql('DROP TABLE lesson');
        $this->addSql('DROP TABLE lesson_user');
        $this->addSql('DROP TABLE document');
        $this->addSql('DROP TABLE tva');
        $this->addSql('DROP TABLE profession_user');
        $this->addSql('DROP TABLE profession_user_profession');
        $this->addSql('DROP TABLE notification');
        $this->addSql('DROP TABLE profession');
        $this->addSql('DROP TABLE address');
        $this->addSql('DROP TABLE comment');
    }
}
