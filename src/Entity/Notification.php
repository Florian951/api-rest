<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\NotificationRepository")
 */
class Notification
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $creationDate;

    /**
     * @ORM\Column(type="boolean")
     */
    private $notificationRead;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Traineeship", inversedBy="notifications",cascade={"remove"})
     */
    private $traineeship;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="notifications",cascade={"remove"})
     */
    private $professional;

  


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreationDate(): ?\DateTimeInterface
    {
        return $this->creationDate;
    }

    public function setCreationDate(\DateTimeInterface $creationDate): self
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    public function getNotificationRead(): ?bool
    {
        return $this->notificationRead;
    }

    public function setNotificationRead(bool $notificationRead): self
    {
        $this->notificationRead = $notificationRead;

        return $this;
    }

    public function getTraineeship(): ?Traineeship
    {
        return $this->traineeship;
    }

    public function setTraineeship(?Traineeship $traineeship): self
    {
        $this->traineeship = $traineeship;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getProfessional(): ?User
    {
        return $this->professional;
    }

    public function setProfessional(?User $professional): self
    {
        $this->professional = $professional;

        return $this;
    }

   

   
}
