<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TvaRepository")
 */
class Tva
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nameTax;

    /**
     * @ORM\Column(type="float")
     */
    private $percentage;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNameTax(): ?string
    {
        return $this->nameTax;
    }

    public function setNameTax(string $nameTax): self
    {
        $this->nameTax = $nameTax;

        return $this;
    }

    public function getPercentage(): ?float
    {
        return $this->percentage;
    }

    public function setPercentage(float $percentage): self
    {
        $this->percentage = $percentage;

        return $this;
    }
}
