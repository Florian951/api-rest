<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity("email")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastName;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $lessonPrice;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $path;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $level;

    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=350, nullable=true)
     */
    private $socialNetwork;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $facebook;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $twitter;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $instagram;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $webSite;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Comment", inversedBy="users")
     */
    private $Comment;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", inversedBy="users")
     */
    private $users;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Address", mappedBy="user")
     */
    private $address;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Document", mappedBy="user")
     */
    private $documents;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Traineeship", inversedBy="users")
     */
    private $traineeships;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProfessionUser", mappedBy="user")
     */
    private $professionUsers;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Lesson", mappedBy="users")
     */
    private $lessons;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Traineeship", mappedBy="user")
     */
    private $traineeshipCreator;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Notification", mappedBy="professional")
     */
    private $notifications;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Equide", mappedBy="User")
     */
    private $equides;

   

    public function __construct()
    {
        $this->Comment = new ArrayCollection();
        $this->address = new ArrayCollection();
        $this->documents = new ArrayCollection();
        $this->traineeships = new ArrayCollection();
        $this->professionUsers = new ArrayCollection();
        $this->lessons = new ArrayCollection();
        $this->traineeshipCreator = new ArrayCollection();
        $this->notifications = new ArrayCollection();
        $this->equides = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        // $roles[] = 'ROLE_PARTICULIER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getPhone(): ?int
    {
        return $this->phone;
    }

    public function setPhone(?int $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getLessonPrice(): ?float
    {
        return $this->lessonPrice;
    }

    public function setLessonPrice(?float $lessonPrice): self
    {
        $this->lessonPrice = $lessonPrice;

        return $this;
    }

    public function getPath(): ?float
    {
        return $this->path;
    }

    public function setPath(?float $path): self
    {
        $this->path = $path;

        return $this;
    }

    public function getLevel(): ?string
    {
        return $this->level;
    }

    public function setLevel(?string $level): self
    {
        $this->level = $level;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getSocialNetwork(): ?string
    {
        return $this->socialNetwork;
    }

    public function setSocialNetwork(?string $socialNetwork): self
    {
        $this->socialNetwork = $socialNetwork;

        return $this;
    }

    public function getFacebook(): ?string
    {
        return $this->facebook;
    }

    public function setFacebook(?string $facebook): self
    {
        $this->facebook = $facebook;

        return $this;
    }

    public function getTwitter(): ?string
    {
        return $this->twitter;
    }

    public function setTwitter(?string $twitter): self
    {
        $this->twitter = $twitter;

        return $this;
    }

    public function getInstagram(): ?string
    {
        return $this->instagram;
    }

    public function setInstagram(?string $instagram): self
    {
        $this->instagram = $instagram;

        return $this;
    }

    public function getWebSite(): ?string
    {
        return $this->webSite;
    }

    public function setWebSite(?string $webSite): self
    {
        $this->webSite = $webSite;

        return $this;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComment(): Collection
    {
        return $this->Comment;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->Comment->contains($comment)) {
            $this->Comment[] = $comment;
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->Comment->contains($comment)) {
            $this->Comment->removeElement($comment);
        }

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(self $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
        }

        return $this;
    }

    public function removeUser(self $user): self
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
        }

        return $this;
    }

   

    /**
     * @return Collection|Address[]
     */
    public function getAddress(): Collection
    {
        return $this->address;
    }

    public function addAddress(Address $address): self
    {
        if (!$this->address->contains($address)) {
            $this->address[] = $address;
            $address->setUser($this);
        }

        return $this;
    }

    public function removeAddress(Address $address): self
    {
        if ($this->address->contains($address)) {
            $this->address->removeElement($address);
            // set the owning side to null (unless already changed)
            if ($address->getUser() === $this) {
                $address->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Document[]
     */
    public function getDocuments(): Collection
    {
        return $this->documents;
    }

    public function addDocument(Document $document): self
    {
        if (!$this->documents->contains($document)) {
            $this->documents[] = $document;
            $document->setUser($this);
        }

        return $this;
    }

    public function removeDocument(Document $document): self
    {
        if ($this->documents->contains($document)) {
            $this->documents->removeElement($document);
            // set the owning side to null (unless already changed)
            if ($document->getUser() === $this) {
                $document->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Traineeship[]
     */
    public function getTraineeships(): Collection
    {
        return $this->traineeships;
    }

    public function addTraineeship(Traineeship $traineeship): self
    {
        if (!$this->traineeships->contains($traineeship)) {
            $this->traineeships[] = $traineeship;
        }

        return $this;
    }

    public function removeTraineeship(Traineeship $traineeship): self
    {
        if ($this->traineeships->contains($traineeship)) {
            $this->traineeships->removeElement($traineeship);
        }

        return $this;
    }

   

  
    /**
     * @return Collection|ProfessionUser[]
     */
    public function getProfessionUsers(): Collection
    {
        return $this->professionUsers;
    }

    public function addProfessionUser(ProfessionUser $professionUser): self
    {
        if (!$this->professionUsers->contains($professionUser)) {
            $this->professionUsers[] = $professionUser;
            $professionUser->setUser($this);
        }

        return $this;
    }

    public function removeProfessionUser(ProfessionUser $professionUser): self
    {
        if ($this->professionUsers->contains($professionUser)) {
            $this->professionUsers->removeElement($professionUser);
            // set the owning side to null (unless already changed)
            if ($professionUser->getUser() === $this) {
                $professionUser->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Lesson[]
     */
    public function getLessons(): Collection
    {
        return $this->lessons;
    }

    public function addLesson(Lesson $lesson): self
    {
        if (!$this->lessons->contains($lesson)) {
            $this->lessons[] = $lesson;
            $lesson->addUser($this);
        }

        return $this;
    }

    public function removeLesson(Lesson $lesson): self
    {
        if ($this->lessons->contains($lesson)) {
            $this->lessons->removeElement($lesson);
            $lesson->removeUser($this);
        }

        return $this;
    }

   

    /**
     * @return Collection|Traineeship[]
     */
    public function getTraineeshipCreator(): Collection
    {
        return $this->traineeshipCreator;
    }

    public function addTraineeshipCreator(Traineeship $traineeshipCreator): self
    {
        if (!$this->traineeshipCreator->contains($traineeshipCreator)) {
            $this->traineeshipCreator[] = $traineeshipCreator;
            $traineeshipCreator->setUser($this);
        }

        return $this;
    }

    public function removeTraineeshipCreator(Traineeship $traineeshipCreator): self
    {
        if ($this->traineeshipCreator->contains($traineeshipCreator)) {
            $this->traineeshipCreator->removeElement($traineeshipCreator);
            // set the owning side to null (unless already changed)
            if ($traineeshipCreator->getUser() === $this) {
                $traineeshipCreator->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Notification[]
     */
    public function getNotifications(): Collection
    {
        return $this->notifications;
    }

    public function addNotification(Notification $notification): self
    {
        if (!$this->notifications->contains($notification)) {
            $this->notifications[] = $notification;
            $notification->setProfessional($this);
        }

        return $this;
    }

    public function removeNotification(Notification $notification): self
    {
        if ($this->notifications->contains($notification)) {
            $this->notifications->removeElement($notification);
            // set the owning side to null (unless already changed)
            if ($notification->getProfessional() === $this) {
                $notification->setProfessional(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Equide[]
     */
    public function getEquides(): Collection
    {
        return $this->equides;
    }

    public function addEquide(Equide $equide): self
    {
        if (!$this->equides->contains($equide)) {
            $this->equides[] = $equide;
            $equide->setUser($this);
        }

        return $this;
    }

    public function removeEquide(Equide $equide): self
    {
        if ($this->equides->contains($equide)) {
            $this->equides->removeElement($equide);
            // set the owning side to null (unless already changed)
            if ($equide->getUser() === $this) {
                $equide->setUser(null);
            }
        }

        return $this;
    }

   
}
