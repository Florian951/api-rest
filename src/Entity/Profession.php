<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProfessionRepository")
 */
class Profession
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\ProfessionUser", mappedBy="professions")
     */
    private $professionUsers;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $picture;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $optionProfession = [];

    /**
     * @ORM\Column(type="array")
     */
    private $optionHour = [];

   

   
    public function __construct()
    {
      
        $this->professionUsers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|ProfessionUser[]
     */
    public function getProfessionUsers(): Collection
    {
        return $this->professionUsers;
    }

    public function addProfessionUser(ProfessionUser $professionUser): self
    {
        if (!$this->professionUsers->contains($professionUser)) {
            $this->professionUsers[] = $professionUser;
            $professionUser->addProfession($this);
        }

        return $this;
    }

    public function removeProfessionUser(ProfessionUser $professionUser): self
    {
        if ($this->professionUsers->contains($professionUser)) {
            $this->professionUsers->removeElement($professionUser);
            $professionUser->removeProfession($this);
        }

        return $this;
    }

    public function getPicture(): ?string
    {
        return $this->picture;
    }

    public function setPicture(string $picture): self
    {
        $this->picture = $picture;

        return $this;
    }

    public function getOptionProfession(): ?array
    {
        return $this->optionProfession;
    }

    public function setOptionProfession(?array $optionProfession): self
    {
        $this->optionProfession = $optionProfession;

        return $this;
    }

    public function getOptionHour(): ?array
    {
        return $this->optionHour;
    }

    public function setOptionHour(array $optionHour): self
    {
        $this->optionHour = $optionHour;

        return $this;
    }

   

    
}
