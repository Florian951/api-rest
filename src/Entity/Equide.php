<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EquideRepository")
 */
class Equide
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $race;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $age;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sexe;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateObtention;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $conditionVie;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $disciplinePratique;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $competition;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $competitonDiscipline;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $niveau;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cavalierActuel;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $objectif;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $chevalSoinEffectuer;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="equides")
     */
    private $User;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRace(): ?string
    {
        return $this->race;
    }

    public function setRace(?string $race): self
    {
        $this->race = $race;

        return $this;
    }

    public function getAge(): ?int
    {
        return $this->age;
    }

    public function setAge(?int $age): self
    {
        $this->age = $age;

        return $this;
    }

    public function getSexe(): ?string
    {
        return $this->sexe;
    }

    public function setSexe(?string $sexe): self
    {
        $this->sexe = $sexe;

        return $this;
    }

    public function getDateObtention(): ?\DateTimeInterface
    {
        return $this->dateObtention;
    }

    public function setDateObtention(?\DateTimeInterface $dateObtention): self
    {
        $this->dateObtention = $dateObtention;

        return $this;
    }

    public function getConditionVie(): ?string
    {
        return $this->conditionVie;
    }

    public function setConditionVie(?string $conditionVie): self
    {
        $this->conditionVie = $conditionVie;

        return $this;
    }

    public function getDisciplinePratique(): ?string
    {
        return $this->disciplinePratique;
    }

    public function setDisciplinePratique(?string $disciplinePratique): self
    {
        $this->disciplinePratique = $disciplinePratique;

        return $this;
    }

    public function getCompetition(): ?bool
    {
        return $this->competition;
    }

    public function setCompetition(?bool $competition): self
    {
        $this->competition = $competition;

        return $this;
    }

    public function getCompetitonDiscipline(): ?string
    {
        return $this->competitonDiscipline;
    }

    public function setCompetitonDiscipline(?string $competitonDiscipline): self
    {
        $this->competitonDiscipline = $competitonDiscipline;

        return $this;
    }

    public function getNiveau(): ?string
    {
        return $this->niveau;
    }

    public function setNiveau(?string $niveau): self
    {
        $this->niveau = $niveau;

        return $this;
    }

    public function getCavalierActuel(): ?string
    {
        return $this->cavalierActuel;
    }

    public function setCavalierActuel(?string $cavalierActuel): self
    {
        $this->cavalierActuel = $cavalierActuel;

        return $this;
    }

    public function getObjectif(): ?string
    {
        return $this->objectif;
    }

    public function setObjectif(?string $objectif): self
    {
        $this->objectif = $objectif;

        return $this;
    }

    public function getChevalSoinEffectuer(): ?string
    {
        return $this->chevalSoinEffectuer;
    }

    public function setChevalSoinEffectuer(?string $chevalSoinEffectuer): self
    {
        $this->chevalSoinEffectuer = $chevalSoinEffectuer;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->User;
    }

    public function setUser(?User $User): self
    {
        $this->User = $User;

        return $this;
    }
}
