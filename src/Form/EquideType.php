<?php

namespace App\Form;

use App\Entity\Equide;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EquideType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('race')
            ->add('age')
            ->add('sexe')
            ->add('dateObtention')
            ->add('conditionVie')
            ->add('disciplinePratique')
            ->add('competition')
            ->add('competitonDiscipline')
            ->add('niveau')
            ->add('cavalierActuel')
            ->add('objectif')
            ->add('chevalSoinEffectuer')
            ->add('User')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Equide::class,
        ]);
    }
}
