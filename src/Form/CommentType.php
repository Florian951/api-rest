<?php

namespace App\Form;

use App\Entity\Comment;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CommentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('note', ChoiceType::class, [
            'choices' => [
            '0/5' => 0,
            '1/5' =>1,
            '2/5' =>2,
            '3/5' =>3,
            '4/5' =>4,
            '5/5' =>5,
            ]])
            ->add('text')
            ->add('Date', DateType::class, [
                'widget' => 'single_text'
            ])
            ->add('author')
            
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Comment::class,
        ]);
    }
}
