<?php

namespace App\Form;

use App\Entity\Traineeship;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TraineeshipType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('program')
            ->add('level')
            ->add('price')
            ->add('end', DateTimeType::class, [
                'widget' => 'single_text'
            ])
            ->add('start', DateTimeType::class, [
                'widget' => 'single_text'
            ])
            ->add('numberParticipant')
            ->add('title')
           
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Traineeship::class,
            'csrf_protection' => false,
            'allow_extra_fields' => true
        ]);
    }
}
