<?php

namespace App\Controller;

use App\Entity\Notification;
use App\Repository\NotificationRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/notification")
 */

class ApiNotificationController extends AbstractController
{
    /**
     * @Route("/search", methods="GET")
     */
    public function findNotificationNotRead(NotificationRepository $notification )
    {
        // $notifications = $this->getUser()->getNotifications()->findBy(["notificationRead" => false]);
        $notifications = $notification->findBy(["professional" => $this->getUser(),"notificationRead" => false]);
        return $this->json($notifications, Response::HTTP_CREATED);
    }

    /**
     * @Route("/read/{notification}", methods="PATCH")
     */
    public function patchForReadNotification(Notification $notification, ObjectManager $manager)
    {
        $notification->setNotificationRead(true);
        $manager->persist($notification);
        $manager->flush();

        return $this->json($notification, Response::HTTP_CREATED);
    }
}
