<?php

namespace App\Controller;

use App\Entity\Lesson;
use App\Entity\Notification;
use App\Form\LessonEventType;
use App\Repository\LessonRepository;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api")
 */

class IndividualLessonController extends AbstractController

{
    /**
     * @Route("/add-event-particular", methods="POST")
     * 
     */
    public function modificationEvent(Request $request, ObjectManager $manager, UserRepository $repoUser, LessonRepository $repoLesson)
    {
        $user = $this->getUser();
        $professionalSelected = $request->query->get('professional');
        $professionalSelected = $repoUser->find($professionalSelected);
        $startLesson = $request->request->get('start');


        $dateLesson = $repoLesson->findBy(["start" => $startLesson]);


        if (!$dateLesson) {


            $notification = new Notification();
            $notification->setCreationDate(new \DateTime());
            $notification->setNotificationRead(false);
            $notification->setName("a souscris à un cours");

            $lesson = new Lesson();

            $form = $this->createForm(LessonEventType::class, $lesson, array(
                'csrf_protection' => false
            ));
            //$form->handleRequest() : a besoin d'un objet
            $form->submit(
                json_decode($request->getContent(), true),
                false
            );

            if ($form->isSubmitted() && $form->isValid()) {

                $lesson->setClassName("color-green");
                $lesson->addUser($user);
                $lesson->addUser($professionalSelected);
                $notification->setProfessional($professionalSelected);

                $manager->persist($notification, $lesson);
                $manager->flush();
            }


            return $this->json($user, Response::HTTP_CREATED);
        }
        return $this->json(Response::HTTP_NOT_MODIFIED);
    }


    /**
     * @Route("/add-event", methods="POST")
     */
    public function addIndividualLessonByPro(Request $request, ObjectManager $manager)
    {

        $lesson = new Lesson();


        $userConnected = $this->getUser();


        $form = $this->createForm(LessonEventType::class, $lesson, array(
            'csrf_protection' => false
        ));
        $form->submit(
            json_decode($request->getContent(), true),
            false
        );
        if ($form->isSubmitted() && $form->isValid()) {

            $lesson->addUser($userConnected);

            $manager->persist($lesson);
            $manager->flush();

            return $this->json($lesson, Response::HTTP_CREATED);
        }


        return $this->json(["message" => "Error" . $form->getErrors(true)], Response::HTTP_BAD_REQUEST);
    }

    /**
     * @Route("/find-lesson", methods="GET")
     */
    public function findAllLessonForCalendarByProfessionalConnected()
    {
        $user = $this->getUser();
        $lessons = $user->getLessons();
        $traineeships = $user->getTraineeshipCreator();
        $collection = new ArrayCollection(
            array_merge($lessons->toArray(), $traineeships->toArray())
        );

        return $this->json($collection, Response::HTTP_OK);
    }

    /**
     * @Route("/find-all-lessons", methods="GET")
     */
    public function findAllLessonForAllUser(LessonRepository $repoLessons)
    {
        $lessons = $repoLessons->findAll();
        return $this->json($lessons, Response::HTTP_FOUND);
    }
}
