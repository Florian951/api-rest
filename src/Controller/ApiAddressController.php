<?php

namespace App\Controller;

use App\Entity\Address;
use App\Form\AddressType;
use App\Repository\AddressRepository;
use App\Repository\TraineeshipRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/address")
 */

class ApiAddressController extends AbstractController
{
    /**
     * @Route(methods="POST")
     * @Route("{addressUser}", methods="PATCH")
     */
    public function addAddressForUserAndHorse($addressUser = null, Request $request, ObjectManager $manager)
    {
        if (!$addressUser) {
            $address = new Address();
        } else {
            $address = $manager->getRepository(Address::class)->find($addressUser);
        }
        $user = $this->getUser();
        $form = $this->createForm(AddressType::class, $address, array(
            'csrf_protection' => false
        ));

        $form->submit(
            json_decode($request->getContent(), true),
            false
        );

        if ($form->isSubmitted() && $form->isValid()) {

            $address->setUser($user);
            $manager->persist($address);
            $manager->flush();

            return $this->json($address, Response::HTTP_CREATED);
        }

        return $this->json(["message" => "Error" . $form->getErrors(true)], Response::HTTP_BAD_REQUEST);
    }

    /**
     * @Route("/traineeship",methods="POST")
     * @Route("/traineeship/{addressTraineeship}", methods="PATCH")
     */
    public function addAddressForTraineeship($addressTraineeship = null, Request $request, ObjectManager $manager, TraineeshipRepository $repoTraineeship)
    {
        if (!$addressTraineeship) {
            $address = new Address();
        } else {
            $address = $manager->getRepository(Address::class)->find($addressTraineeship);
        }
        $traineeshipSelected = $request->query->get('traineeship');
        $traineeshipSelected = $repoTraineeship->find($traineeshipSelected);

        $form = $this->createForm(AddressType::class, $address, array(
            'csrf_protection' => false
        ));

        $form->submit(
            json_decode($request->getContent(), true),
            false
        );

        if ($form->isSubmitted() && $form->isValid()) {

            $address->setTraineeship($traineeshipSelected);
            $manager->persist($address);
            $manager->flush();

            return $this->json($address, Response::HTTP_CREATED);
        }

        return $this->json(["message" => "Error" . $form->getErrors(true)], Response::HTTP_BAD_REQUEST);
    }



    /**
     * @Route("/find-horse", methods="GET")
     */
    public function findAllAddressHorse(AddressRepository $adressRepository)
    {

        $allHorseByPro = $adressRepository->findBy(["user" => $this->getUser(), "adressHorse" => true]);
        return $this->json($allHorseByPro, Response::HTTP_FOUND);
    }
}
