<?php

namespace App\Controller;

use App\Entity\Address;
use App\Entity\Notification;
use App\Entity\Traineeship;
use App\Form\AddressType;
use App\Form\TraineeshipType;
use App\Repository\AddressRepository;
use App\Repository\TraineeshipRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/traineeship")
 */

class TraineeshipController extends AbstractController
{

    /**
     * @Route("/add-address/{traineeship}", methods="POST")
     * @Route("/patch-address/{traineeship}", methods="PATCH")
     */
    public function addAddressForTraineeship(Address $address = null, $traineeship, Request $request, ObjectManager $manager)
    {
        if (!$address) {
            $address = new Address();
        }

        $form = $this->createForm(AddressType::class, $address, array(
            'csrf_protection' => false
        ));

        $form->submit(
            json_decode($request->getContent(), true),
            false
        );

        if ($form->isSubmitted() && $form->isValid()) {

            $address->setTraineeship($traineeship);
            $manager->persist($address);
            $manager->flush();

            return $this->json($address, Response::HTTP_CREATED);
        }

        return $this->json(["message" => "Error" . $form->getErrors(true)], Response::HTTP_BAD_REQUEST);
    }

    /**
     * @Route("/add", methods="POST")
     * @Route("/modify/{traineeship}", methods="PATCH")
     */
    public function addAndMoficationTraineeship(Traineeship $traineeship = null, Request $request, ObjectManager $manager)
    {
        $user = $this->getUser();
        if (!$traineeship) {
            $traineeship = new Traineeship();
        }

        $form = $this->createForm(TraineeshipType::class, $traineeship, array(
            'csrf_protection' => false
        ));

        $form->submit(
            json_decode($request->getContent(), true),
            false
        );

        if ($form->isSubmitted() && $form->isValid()) {

            
            $traineeship->setUser($user);
            $manager->persist($traineeship);
            $manager->flush();

            return $this->json($traineeship, Response::HTTP_CREATED);
        }

        return $this->json(["message" => "Error" . $form->getErrors(true)], Response::HTTP_BAD_REQUEST);
    }

    /**
     * @Route("/register-particular/{traineeship}", methods="GET")
     */
    public function addUserToTraineeship($traineeship, TraineeshipRepository $repoTraineeship, ObjectManager $manager)
    {
        $traineeshipSelected = $repoTraineeship->find($traineeship);
        $totalUsers = $traineeshipSelected->getUsers();
        $points = 0;

        foreach ($totalUsers as $task) {
            $points++;
        }

        if ($points < $traineeshipSelected->getNumberParticipant()) {

            $notification = new Notification();
            $notification->setCreationDate(new \DateTime());
            $notification->setNotificationRead(false);
            $notification->setName('Un nouvelle utilisateur a rejoind votre stage');


            $user = $this->getUser();
            $user->addTraineeship($traineeshipSelected);
            $userCreator = $traineeshipSelected->getUser();
            $traineeshipSelected->addNotification($notification);
            $traineeshipSelected->addUser($userCreator);     
            $notification->setProfessional($userCreator);
            $user->addUser($userCreator);
            $userCreator->addUser($user);

            $manager->persist($notification, $user);
            $manager->flush();

            return $this->json($user, Response::HTTP_CREATED);
        }

        return $this->json(Response::HTTP_NOT_MODIFIED);
    }

    /**
     * @Route("/find-all", methods="GET")
     */
    public function findAllTraineeshipForUserAndProConnected()
    {
        $user =  $this->getUser()->getTraineeshipCreator();
        return $this->json($user, Response::HTTP_CREATED);
    }

    /**
     * @Route("/find-all-traineeship", methods="GET")
     */
    public function findAllTraineeshipForAllUserNotConnected(TraineeshipRepository $repoTraineeship)
    {
        $traineeships = $repoTraineeship->findAll();
        return $this->json($traineeships, Response::HTTP_FOUND);
    }

    /**
     * @Route("/search-traineeship/{search}", methods="GET")
     */
    public function findAllTraineeshipWhithSearch($search, TraineeshipRepository $repoTraineeship)
    {
        $traineeships = $repoTraineeship->findTraineeship($search);

        return $this->json($traineeships, Response::HTTP_FOUND);
    }
    /**
     * @Route("/find-one/{idTraineeship}", methods="GET")
     */
    public function findOneTraineeshipForUserAndPro($idTraineeship, TraineeshipRepository $repoTraineeship)
    {
        $traineeship =  $repoTraineeship->findBy(["user" => $this->getUser(),"id" => $idTraineeship]);
        return $this->json($traineeship, Response::HTTP_OK);
    }

    /**
     * @Route("/delete/{id}", methods="DELETE")
     */
    public function deleteTraineeshipByProfessionnal($id, TraineeshipRepository $traineeshipRepository,AddressRepository $addressRepo, ObjectManager $manager)
    {   
    
        $traineeshipSelected = $traineeshipRepository->find($id);
        $traineeshipSelected->setUser(null);
        $manager->remove($traineeshipSelected);
        $manager->flush();

        return $this->json(Response::HTTP_OK);
    }
}
