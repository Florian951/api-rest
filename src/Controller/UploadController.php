<?php

namespace App\Controller;

use App\Entity\Document;
use App\Entity\Traineeship;
use App\Repository\DocumentRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UploadController extends AbstractController
{
  /**
   * @Route("/api/upload", methods="POST")
   */
  public function upload(ObjectManager $manager)
  {

    $request = Request::createFromGlobals();

    $uploadedFile = $request->files->all()['file'];
    $nameFile = $uploadedFile->getClientOriginalName();
    $fileName = $nameFile . '-' . uniqid() . '.' . $uploadedFile->guessExtension();


    $uploadedFile->move(__DIR__ . '/../../public/pictures', $fileName);
    //Repo
    $donnees = $request->request->get('traineeship');
    $traineeship = $manager->getRepository(Traineeship::class)->find($donnees);
    $document = new Document();
    $document->setTraineeship($traineeship);
    $document->setName($fileName);
    $document->setType($uploadedFile->guessExtension());
    //Persist
    $manager->persist($document);
    $manager->flush();

    return $this->json($donnees, Response::HTTP_CREATED);
  }

  /**
   * @Route("/api/upload/find/{idTraineeship}", methods="GET")
   */
  public function findDocument($idTraineeship, DocumentRepository $repoDocument)
  {

    $documents = $repoDocument->findBy(["traineeship" => $idTraineeship, "user" => $this->getUser()]);

    return $this->json($documents, Response::HTTP_OK);
  }

  /**
   * @Route("/api/upload/delete/{idTraineeship}", methods="DELETE")
   */
  public function deleteDocument($idTraineeship, DocumentRepository $repoDocument, ObjectManager $manager)
  {

    $document = $repoDocument->findBy(["user" => $this->getUser(), "id" => $idTraineeship]);
    $manager->remove($document);
    $manager->flush();

    return $this->json(Response::HTTP_OK);
  }
}
