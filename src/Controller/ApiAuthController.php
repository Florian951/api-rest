<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\PatchUserType;
use App\Form\UserType;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/api")
 */
class ApiAuthController extends AbstractController
{

    /**
     * @Route("/register", methods="POST")
     */
    public function registerUser(Request $request, ObjectManager $om, UserPasswordEncoderInterface $encoder)
    {

        $user = new User();


        $form = $this->createForm(UserType::class, $user, array(
            'csrf_protection' => false
        ));
        //$form->handleRequest() : a besoin d'un objet
        $form->submit(
            json_decode($request->getContent(), true),
            false
        );

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setRoles(["ROLE_PARTICULIER"]);
            $hashPassword = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($hashPassword);

            $om->persist($user);
            $om->flush();

            //return $this->json($user, 201);
            return $this->json(Response::HTTP_CREATED);
        }

        return $this->json(["message" => "user not registred " . $form->getErrors(true)], Response::HTTP_BAD_REQUEST);
    }

    /**
     * @Route("/register-pro", methods="POST")
     */
    public function registerPro(Request $request, ObjectManager $om, UserPasswordEncoderInterface $encoder)
    {

        $user1 = new User();


        $form = $this->createForm(UserType::class, $user1, array(
            'csrf_protection' => false
        ));
        //$form->handleRequest() : a besoin d'un objet
        $form->submit(
            json_decode($request->getContent(), true),
            false
        );

        if ($form->isSubmitted() && $form->isValid()) {
            $user1->setRoles(["ROLE_PRO"]);
            $hashPassword = $encoder->encodePassword($user1, $user1->getPassword());
            $user1->setPassword($hashPassword);

            $om->persist($user1);
            $om->flush();

            //return $this->json($user, 201);
            return $this->json(Response::HTTP_CREATED);
        }

        return $this->json(["message" => "user not registred " . $form->getErrors(true)], Response::HTTP_BAD_REQUEST);
    }


    /**
     * @Route("/modification-social/{id}", methods="PATCH")
     */
    public function modificationProfilUserForSocialNetwork(User $user, Request $request, ObjectManager $om, UserPasswordEncoderInterface $encoder)
    {


        $form = $this->createForm(PatchUserType::class, $user, array(
            'csrf_protection' => false
        ));
        //$form->handleRequest() : a besoin d'un objet
        $form->submit(
            json_decode($request->getContent(), true),
            false
        );

        if ($form->isSubmitted() && $form->isValid()) {
            $om->persist($user);
            $om->flush();
            return $this->json(Response::HTTP_CREATED);
        }

        return $this->json(["message" => "user non modifié" . $form->getErrors(true)], Response::HTTP_BAD_REQUEST);
    }

    /**
     * @Route("/password-patch/{id}", methods="PATCH")
     */
    public function modificationPasswordForUserAndPro(User $user, Request $request, ObjectManager $om, UserPasswordEncoderInterface $encoder)
    {


        $form = $this->createForm(PatchUserType::class, $user, array(
            'csrf_protection' => false
        ));
        //$form->handleRequest() : a besoin d'un objet
        $form->submit(
            json_decode($request->getContent(), true),
            false
        );

        if ($form->isSubmitted() && $form->isValid()) {
            $hashPassword = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($hashPassword);
            $om->persist($user);
            $om->flush();

            //return $this->json($user, 201);
            return $this->json(Response::HTTP_CREATED);
        }

        return $this->json(["message" => "mot de passe invalide" . $form->getErrors(true)], Response::HTTP_BAD_REQUEST);
    }



    /**
     * @Route("/user", methods="GET")
     */
    public function currentUser()
    {
        $user = $this->getUser();
        $user->setPassword("");


        return $this->json($user, Response::HTTP_FOUND);
    }

    /**
     * @Route("/findAllUser", methods="GET")
     */
    public function findAllUserForAdmin(UserRepository $repo)
    {
        $users = $repo->findAll();

        return $this->json($users, Response::HTTP_FOUND);
    }

    /**
     * @Route("/find-old-users", methods="GET")
     */
    public function findAllOldClientForPro()
    {

        $oldUsers = $this->getUser()->getUsers();

        return $this->json($oldUsers, Response::HTTP_FOUND);
    }

    /**
     * @Route("/register/delete/{id}", methods="DELETE")
     */

    public function deleteClientByUser(User $user, ObjectManager $manager)
    {
        $manager->remove($user);
        $manager->flush();
        return $this->json(null, Response::HTTP_OK);
    }
}
