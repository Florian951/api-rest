## How To Use

On commence par faire son .env.local dans lequel on met les infos de connexion

1. `bin/console do:da:dr --force` on drop la base
2. `bin/console do:da:cr` on crée la base
3. `bin/console do:mi:mi` on fait le migrate
4. `bin/console do:fi:lo` on charge les fixtures
5. `bin/console doctrine:schema:update --dump-sql` mise à jour SQL 
6. `bin/console doctrine:schema:update --force` on valide avec un force
7. On crée le couple clef privée/clef publique 

```
mkdir -p config/jwt
openssl genpkey -out config/jwt/private.pem -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096
openssl pkey -in config/jwt/private.pem -out config/jwt/public.pem -pubout
```

6. On modifie le .env.local pour y rajouter la `JWT_PASSPHRASE=passphrase` en remplaçant passphrase par ce que vous avez mis lors de la génération de la clef privée

## Authentification

Pour récupérer le token on fait un post sur http://localhost:8000/api/login_check en lui mettant un header "Content-Type":"application/json" et en lui mettant comme body :

```
{"username":"bidon@mail.com", "password":"1234"}
```

(remplacer les valeurs par celles de vos users, comme on voit, malgré le fait qu'on s'authentifie avec le mail, on utilise la clef "username" quand même dans tous les cas)

Pour utiliser le token, on rajoute un header à nos requête de type "Authorization" qui aura comme valeur "Bearer `letoken`"